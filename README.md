# Get SPDX-License-Identifier from kernel source files

## Usage
Get SPDX-License-Identifier from the current working tree:

    $ kspdx.py include/linux/can/core.h
    GPL-2.0-only OR BSD-3-Clause

    $ kspdx.py arch/x86/
    GPL-2.0-only
    GPL-2.0-only OR BSD-3-Clause
    GPL-2.0-only WITH Linux-syscall-note
    GPL-2.0-or-later
    GPL-2.0-or-later WITH Linux-syscall-note
    LGPL-2.0-or-later WITH Linux-syscall-note
    MIT OR GPL-2.0-only

Get SPDX-License-Identifier from 'origin/master' git state:

    $ kspdx.py -c origin/master arch/x86/kvm/x86.c
    GPL-2.0-only

By default, the tool prints all unique licenses found, '-i' switch can
be used to output licenses per file:

    $ kspdx.py -i drivers/net/ethernet/fujitsu/
    drivers/net/ethernet/fujitsu/Kconfig: GPL-2.0-only
    drivers/net/ethernet/fujitsu/fmvj18x_cs.c: GPL-2.0-only
    drivers/net/ethernet/fujitsu/Makefile: GPL-2.0-only

Note: GPLv2-only license is assumed by default, '-d' switch can be used
to change that:

    $ kspdx.py -d Unknown -i arch/x86/hyperv/
    arch/x86/hyperv/hv_apic.c: GPL-2.0-only
    arch/x86/hyperv/mmu.c: Unknown
    ...

A single joint statement can be printed with '-j' switch:

    $ kspdx.py -j drivers/net/ethernet/
    BSD-3-Clause AND GPL-1.0-or-later AND GPL-2.0-only AND (GPL-2.0-only OR BSD-2-Clause) AND (GPL-2.0-only OR BSD-3-Clause) AND GPL-2.0-or-later AND (GPL-2.0-or-later OR BSD-3-Clause) AND (Linux-OpenIB OR GPL-2.0-only) AND (Linux-OpenIB OR GPL-2.0-only OR BSD-2-Clause) AND (MIT OR GPL-2.0-only)
